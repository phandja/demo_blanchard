package com.demoapp.blanchard.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demoapp.blanchard.demo.entities.Person;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long>{

}
