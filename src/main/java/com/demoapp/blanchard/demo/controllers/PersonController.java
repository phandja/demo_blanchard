package com.demoapp.blanchard.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.demoapp.blanchard.demo.services.PersonService;

@Controller
public class PersonController {
	@Autowired
	private PersonService personService;

	@GetMapping("/")
	public String home(Model model) {
		return "index";
	}

	@GetMapping("/persons")
	public String showPersons(Model model) {
		model.addAttribute("persons", personService.findAll());
		return "persons";
	}

	@GetMapping("/find/{id}")
	public String findPerson(@PathVariable("id") long id, Model model) {
		model.addAttribute("person", personService.findById(id));
		return "person";
	}
	
	@GetMapping("/delete/{id}")
	public String deleteUser(@PathVariable("id") long id, Model model) {
		// Person person = personService.findById(id).orElseThrow(() -> new
		// IllegalArgumentException("Invalid person Id:" + id));
		// personService.delete(person);
		// model.addAttribute("persons", personService.findAll());
		return "persons";
	}

	@GetMapping("/edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		//Person person = personService.findById(id)
		//		.orElseThrow(() -> new IllegalArgumentException("Invalid person Id:" + id));
		//model.addAttribute("person", person);
		return "persons";
	}

}
