package com.demoapp.blanchard.demo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demoapp.blanchard.demo.entities.Person;
import com.demoapp.blanchard.demo.repositories.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService{
	
	@Autowired
	private PersonRepository repository;
	
	@Override
	public List<Person> findAll() {
		List<Person> persons = new ArrayList<>();
		repository.findAll().forEach(persons::add);
		return persons;
	}

	@Override
	public Person findByName(String name) {
		return null;
	}

	@Override
	public Person findById(Long id) {
		Optional<Person> person = repository.findById(id);
		return person.orElse(null);

	}
}
