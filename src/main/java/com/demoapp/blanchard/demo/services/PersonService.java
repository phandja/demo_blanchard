package com.demoapp.blanchard.demo.services;

import java.util.List;
import com.demoapp.blanchard.demo.entities.Person;

public interface PersonService {
	List<Person> findAll();
	Person findByName(String name);
	Person findById(Long id);
}
